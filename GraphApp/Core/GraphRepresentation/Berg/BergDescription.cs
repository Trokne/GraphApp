﻿using GraphApp.Core.GraphApplications;
using GraphApp.Core.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphRepresentation.Berg
{
    public abstract class BergDescription
    {
        /* - - Внутреннее представление графа - - - - - - */
        protected string[] _nameArray;
        protected Edge[,] _edgesArray;
        protected int[] _edgesCountArray;
        public abstract int EdgesCount { get; }

        /* - - Приложения - - - - - - - - - - - - - - - - */
        protected FloydAlgrorithm _floydAlgrorithm;

        public BergDescription()
        {
            _nameArray = new string[0];
            _edgesArray = new Edge[0, 0];
            _edgesCountArray = new int[0];
            InitializeDefaultApplications();
        }

        #region Операции с вершинами
        public int VertexCount() => 
            _nameArray.Length == 0 ? 0 : _nameArray.Where(x => !String.IsNullOrEmpty(x)).Count();
        public int FindVertex(string vertexLabel) => 
            Array.FindIndex(_nameArray, n => n != null && n == vertexLabel);
        public string FindVertexLabel(int index) =>
            index >= _nameArray.Length || index < 0 ? null : _nameArray[index];
        public bool IsExistsVertex(string label) => 
            FindVertex(label) >= 0 ? true : false;
        public int FullArrayCount() => 
            _nameArray.Length == 0 ? 0 : _nameArray.Count();
        public int GraphPower() => 
            _nameArray.Length == 0 ? 0 : _edgesCountArray.Max();
        public int FindRootIndex()
        {
            bool isContain;
            for (int i = 0; i < FullArrayCount(); i++)
            {
                if (_nameArray[i] == null)
                    continue;
                isContain = false;
                for (int j = 0; j < FullArrayCount() && !isContain; j++)
                {
                    for (int k = 0; k < _edgesCountArray[j] && !isContain; k++)
                    {
                        if (_edgesArray[j, k].OutputIndex == i)
                            isContain = true;
                    }
                }
                if (!isContain) return i;
            }
            return -1;
        }
        public bool IsExistsVertex(int vertexIndex)
            => !(vertexIndex >= FullArrayCount() ||
                String.IsNullOrEmpty(_nameArray[vertexIndex]));

        public virtual bool RemoveVertex(string label)
            => RemoveVertex(FindVertex(label));
        public virtual bool AddVertex(string label)
        {
            var index = FindVertex(label);
            if (index >= 0) return false;
            index = Array.FindIndex(_nameArray, x => String.IsNullOrEmpty(x));
            if (index < 0)
            {
                index = FullArrayCount();
                Array.Resize(ref _nameArray, index + 1);
                Array.Resize(ref _edgesCountArray, index + 1);
                _edgesArray = _edgesArray.ResizeArray(index + 1, index + 1);
            }
            _nameArray[index] = label;
            _floydAlgrorithm.IsNeedRecalculate = true;
            return true;
        }
        public virtual bool RemoveVertex(int index)
        {
            if (index < 0) return false;
            _floydAlgrorithm.IsNeedRecalculate = true;
            var sourceLabel = FindVertexLabel(index);
            for (int i = 0; i < FullArrayCount(); i++)
            {
                if (i != index)
                {
                    var targetLabel = FindVertexLabel(i);
                    RemoveEdge(sourceLabel, targetLabel);
                }
            }
            _nameArray[index] = null;
            return true;
        }

        #endregion

        #region Операции с ребрами
        public abstract bool AddEdge(string inputVertex, string outputVertex, double value);
        public abstract bool RemoveEdge(string inputVertex, string outputVertex);

        public Edge GetEdge(string vertex1, string vertex2)
        {
            int vertexIndex1 = FindVertex(vertex1);
            if (vertexIndex1 == -1) return null;
            int vertexIndex2 = FindVertex(vertex2);
            if (vertexIndex2 == -1) return null;
            return GetEdge(vertexIndex1, vertexIndex2);
        }

        public IEnumerable<Edge> GetEdges(int vertex)
        {
            for (int i = 0; i < _edgesCountArray[vertex]; i++)
            {
                if (_edgesArray[vertex, i] == null) continue;
                yield return _edgesArray[vertex, i];
            }
        }

        public Edge GetEdge(int vertex1, int vertex2)
        {
            for (int i = 0; i < _edgesCountArray[vertex1]; i++)
                if (_edgesArray[vertex1, i].OutputIndex == vertex2)
                    return _edgesArray[vertex1, i];
            return null;
        }

        public IEnumerable<Edge> GetEdges(string vertex)
        {
            int index = FindVertex(vertex);
            if (index == -1) return null;
            else return GetEdges(index);
        }

        public List<Edge> GetEdges()
        {
            var edges = new List<Edge>();
            for (int i = 0; i < FullArrayCount(); i++)
            {
                if (!IsExistsVertex(i)) continue;
                foreach (var edge in GetEdges(i))
                    edges.Add(edge);
            }
            return edges;
        }
        #endregion

        #region Операции с приложениями
        public void InitializeDefaultApplications()
        {
            _floydAlgrorithm = new FloydAlgrorithm(this);
        }
        public string Visualize() => GraphVizualizer.GetGraphImgPath(this);
        public double Diameter() => _floydAlgrorithm.Diameter();
        public double Radius() => _floydAlgrorithm.Radius();

        #endregion
    }
}
