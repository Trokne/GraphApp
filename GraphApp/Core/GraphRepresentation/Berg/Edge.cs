﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphRepresentation
{
    public class Edge
    {
        public int InputIndex { get; set; }
        public int OutputIndex { get; set; }
        public double Value { get; set; }

        public Edge(int inputIndex, int outputIndex, double value = 1)
        {
            InputIndex = inputIndex;
            OutputIndex = outputIndex;
            Value = value;
        }
    }
}
