﻿using GraphApp.Core.GraphApplications;
using GraphApp.Core.GraphApplications.Gamma;
using GraphApp.Core.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphRepresentation.Berg
{
    public class BergUndirected : BergDescription
    {
        private DephFirstSearch _dfsAlgorithm;
        private GammaAlgorithm _gammaAlgorithm;
        private AbsoluteCenter _hakimiAlgorithm;

        public BergUndirected() : base()
        {
            _dfsAlgorithm = new DephFirstSearch(this);
            _gammaAlgorithm = new GammaAlgorithm(this);
            _hakimiAlgorithm = new AbsoluteCenter(this);
        }

        public BergUndirected(BergDescription sourceGraph) : base()
        {
            _dfsAlgorithm = new DephFirstSearch(this);
            _hakimiAlgorithm = new AbsoluteCenter(this);
            _gammaAlgorithm = new GammaAlgorithm(this);
            InitializeFromGraph(sourceGraph);
        }

        private void InitializeFromGraph(BergDescription sourceGraph)
        {
            for (int i = 0; i < sourceGraph.FullArrayCount(); i++)
            {
                if (!sourceGraph.IsExistsVertex(i)) continue;
                AddVertex(sourceGraph.FindVertexLabel(i));
            }

            for (int i = 0; i < sourceGraph.FullArrayCount(); i++)
            {
                if (!sourceGraph.IsExistsVertex(i)) continue;
                string inputLabel = sourceGraph.FindVertexLabel(i);
                foreach (var edge in sourceGraph.GetEdges(i))
                {
                    string outputLabel = sourceGraph.FindVertexLabel(edge.OutputIndex);
                    var inverseEdge = GetEdge(outputLabel, inputLabel);
                    if (!(inverseEdge != null && sourceGraph is BergDirected))
                        AddEdge(inputLabel, outputLabel, edge.Value);
                }
            }
        }

        public BergUndirected(string[] nameArray, Edge[,] edgesArray, int[] edgesCountArray)
        {
            _dfsAlgorithm = new DephFirstSearch(this);
            _gammaAlgorithm = new GammaAlgorithm(this);
            _hakimiAlgorithm = new AbsoluteCenter(this);
            _nameArray = nameArray;
            _edgesArray = edgesArray;
            _edgesCountArray = edgesCountArray;
            InitializeDefaultApplications();
        }

        #region Работа с приложениями
        public int CyclomaticNumber() => VertexCount() == 0 ? 0 : ConnectivityComponents() + EdgesCount - VertexCount();
        public int ConnectivityComponents() => _dfsAlgorithm.ConnectivityComponents();
        public bool IsContainCycle() => _dfsAlgorithm.IsContainCycle();
        public bool IsTree() => _dfsAlgorithm.IsTree();
        public List<int> GetCycle() => _dfsAlgorithm.GetCycle();
        public bool IsPlanar() => _gammaAlgorithm.IsPlanar();
        public double[,] GetDistanceMatrix() => _floydAlgrorithm.GetDistanceMatrix();
        public string GetAbsoluteCenter() => _hakimiAlgorithm.GetAbsoluteCenter();
        #endregion

        #region Операции с ребрами

        private int _edgesCount;

        public List<Edge> GetDistinctEdges()
        {
            var edges = new List<Edge>();
            
            for (int i = 0; i < FullArrayCount(); i++)
            {
                if (!IsExistsVertex(i)) continue;
                foreach (var edge in GetEdges(i))
                {
                    if (!edges.Any(e => 
                        (e.InputIndex == edge.InputIndex && e.OutputIndex == edge.OutputIndex) ||
                        (e.OutputIndex == edge.InputIndex && e.InputIndex == edge.OutputIndex)
                        ))
                    {
                        edges.Add(edge);
                    }
                }
            }

            return edges;
        }

        public override int EdgesCount => _edgesCount;

        public override bool AddEdge(string inputVertex, string outputVertex, double value = 1)
        {
            int inputIndex = Array.FindIndex(_nameArray, n => n == inputVertex);
            if (inputIndex == -1) return false;
            int outputIndex = Array.FindIndex(_nameArray, n => n == outputVertex);
            if (outputIndex == -1) return false;
            if (GetEdge(inputIndex, outputIndex) != null) return false;
            AddEdge(inputIndex, outputIndex, value);
            AddEdge(outputIndex, inputIndex, value);
            _floydAlgrorithm.IsNeedRecalculate = true;
            _dfsAlgorithm.IsNeedExecute = true;
            _edgesCount++;
            return true;
        }

        private void AddEdge(int inputIndex, int outputIndex, double value)
        {
            var edge = new Edge(inputIndex, outputIndex, value);
            var newEdgeIndex = _edgesCountArray[inputIndex];
            _edgesArray[inputIndex, newEdgeIndex] = edge;
            _edgesCountArray[inputIndex]++;
        }

        public override bool RemoveEdge(string inputVertex, string outputVertex)
        {
            int inputIndex = Array.FindIndex(_nameArray, n => n == inputVertex);
            if (inputIndex == -1) return false;
            int outputIndex = Array.FindIndex(_nameArray, n => n == outputVertex);
            if (outputIndex == -1) return false;
            _floydAlgrorithm.IsNeedRecalculate = true;
            _dfsAlgorithm.IsNeedExecute = true;
            if (RemoveEdge(inputIndex, outputIndex) &&
               RemoveEdge(outputIndex, inputIndex))
            {
                _edgesCount--;
                return true;
            }
            return false;
        }

        private bool RemoveEdge(int inputIndex, int outputIndex)
        {
            for (int i = 0; i < _edgesCountArray[inputIndex]; i++)
            {
                if (_edgesArray[inputIndex, i].OutputIndex == outputIndex)
                {
                    _edgesArray[inputIndex, i] = null;
                    _edgesCountArray[inputIndex]--;
                    if (i + 1 < _edgesArray.GetLength(1) &&
                    _edgesArray[inputIndex, i + 1] != null)
                        _edgesArray = _edgesArray.MoveArray(inputIndex, i, 1, ArrayHelper.Direction.Left);
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Операции с вершинами
        public override bool AddVertex(string label)
        {
            bool isGoodResult = base.AddVertex(label);
            if (isGoodResult) _dfsAlgorithm.IsNeedExecute = true;
            return isGoodResult;
        }
        public override bool RemoveVertex(string label)
        {
            bool isGoodResult = base.RemoveVertex(label);
            if (isGoodResult) _dfsAlgorithm.IsNeedExecute = true;
            return isGoodResult;
        }
        public override bool RemoveVertex(int index)
        {
            bool isGoodResult = base.RemoveVertex(index);
            if (isGoodResult) _dfsAlgorithm.IsNeedExecute = true;
            return isGoodResult;
        }
        #endregion
    }
}
