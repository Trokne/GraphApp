﻿using GraphApp.Core.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphRepresentation.Berg
{
    public class BergDirected : BergDescription
    {
        public BergDirected() : base()
        {
            
        }

        public BergDirected(BergDescription sourceGraph) : base()
        {
            for (int i = 0; i < sourceGraph.FullArrayCount(); i++)
            {
                if (!sourceGraph.IsExistsVertex(i)) continue;
                AddVertex(sourceGraph.FindVertexLabel(i));
            }

            for (int i = 0; i < sourceGraph.FullArrayCount(); i++)
            {
                if (!sourceGraph.IsExistsVertex(i)) continue;
                string inputLabel = sourceGraph.FindVertexLabel(i);
                foreach (var edge in sourceGraph.GetEdges(i))
                {
                    string outputLabel = sourceGraph.FindVertexLabel(edge.OutputIndex);
                    AddEdge(inputLabel, outputLabel, edge.Value);
                }
            }

        }

        public BergDirected(string[] nameArray, Edge[,] edgesArray, int[] edgesCountArray)
        {
            _nameArray = nameArray;
            _edgesArray = edgesArray;
            _edgesCountArray = edgesCountArray;
            InitializeDefaultApplications();
        }

        #region Операции с приложениями

        #endregion

        #region Операции с ребрами

        public override int EdgesCount => _nameArray.Length == 0 ? 0 : _edgesCountArray.Sum(x => x);

        public override bool AddEdge(string inputVertex, string outputVertex, double value)
        {
            int inputIndex = Array.FindIndex(_nameArray, n => n == inputVertex);
            if (inputIndex == -1) return false;
            int outputIndex = Array.FindIndex(_nameArray, n => n == outputVertex);
            if (outputIndex == -1) return false;
            var existsEdge = GetEdge(inputIndex, outputIndex);
            if (existsEdge != null) return false;
            var edge = new Edge(inputIndex, outputIndex, value);
            var newEdgeIndex = _edgesCountArray[inputIndex];
            _edgesArray[inputIndex, newEdgeIndex] = edge;
            _edgesCountArray[inputIndex]++;
            _floydAlgrorithm.IsNeedRecalculate = true;
            return true;
        }

        public override bool RemoveEdge(string inputVertex, string outputVertex)
        {
            int inputIndex = Array.FindIndex(_nameArray, n => n == inputVertex);
            if (inputIndex == -1) return false;
            int outputIndex = Array.FindIndex(_nameArray, n => n == outputVertex);
            if (outputIndex == -1) return false;
            _floydAlgrorithm.IsNeedRecalculate = true;
            for (int i = 0; i < _edgesCountArray[inputIndex]; i++)
            {
                if (_edgesArray[inputIndex, i].OutputIndex == outputIndex)
                {
                    _edgesArray[inputIndex, i] = null;
                    _edgesCountArray[inputIndex]--;
                    if (i + 1 < _edgesArray.GetLength(1) &&
                    _edgesArray[inputIndex, i + 1] != null)
                        _edgesArray = _edgesArray.MoveArray(inputIndex, i, 1, ArrayHelper.Direction.Left);
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Операции с вершинами

        //public override bool RemoveVertex(int index)
        //{
        //    if (index < 0) return false;
        //    _floydAlgrorithm.IsNeedRecalculate = true;
        //    _nameArray[index] = null;
        //    for (int j = 0; j < _edgesCountArray[index]; j++)
        //        _edgesArray[index, j] = null;
        //    _edgesCountArray[index] = 0;
        //    for (int i = 0; i < FullArrayCount(); i++)
        //    {
        //        if (i == index) continue;
        //        int offsetIndex = -1;
        //        for (int j = 0; j < _edgesCountArray[i]; j++)
        //        {
        //            if (_edgesArray[i, j].OutputIndex == index)
        //            {
        //                _edgesArray[i, j] = null;
        //                offsetIndex = j;
        //                break;
        //            }
        //        }
        //        if (offsetIndex != -1)
        //        {
        //            _edgesCountArray[i]--;
        //            if (offsetIndex + 1 < _edgesArray.GetLength(1) &&
        //            _edgesArray[i, offsetIndex + 1] != null)
        //            {
        //                _edgesArray = _edgesArray.MoveArray(i, offsetIndex, 1, ArrayHelper.Direction.Left);
        //            }
        //        }
        //    }
        //    return true;
        //}

        #endregion

    }
}
