﻿using GraphApp.Core.GraphRepresentation.Berg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphRepresentation.Description
{
    public class GraphInformation
    {
        public static BergDescription Graph { get; set; }


        public enum GeneralOperations
        {
            [Description("Сменить ориентацию графа")]
            GraphType,
            [Description("Список вершин")]
            VertexList,
            [Description("Добавление вершины")]
            AddVertex,
            [Description("Удаление вершины")]
            RemoveVertex,
            [Description("Добавление ребра")]
            AddEdge,
            [Description("Удаление ребра")]
            RemoveEdge,
            [Description("Проверка существования ребра")]
            FindEdge,
            [Description("Поиск смежных вершин")]
            FindAdjVertices
        }

        public enum UndirectedOperations
        {
            [Description("Обход в глубину")]
            VertexListByDFS = GeneralOperations.FindAdjVertices + 1,
            [Description("Вывести цикл")]
            PrintCycle,
            [Description("Планарность графа")]
            IsPlanar,
            [Description("Абсолютный центр")]
            AbsoluteCenter
        }

        public enum GraphType
        {
            [Description("Описание Бержа")]
            BergDescription,
            [Description("Матрица смежности")]
            AdjacencyMatrix,
            [Description("Матрица инцидентности")]
            IncidenceMatrix,
            [Description("Список дуг")]
            ArcsList
        }
    }
}
