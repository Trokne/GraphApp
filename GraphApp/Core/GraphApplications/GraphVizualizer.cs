﻿using GraphApp.Core.GraphRepresentation;
using GraphApp.Core.GraphRepresentation.Berg;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphApplications
{
    public static class GraphVizualizer
    {
        private static readonly string PATH = @"C:\tempGraphApp";
        private static readonly string FILE_NAME = @"graphImg";
        private readonly static string DOT_EDGE_DEFAULT = "--";
        private readonly static string DOT_EDGE_ARROW = "->";

        public static string GetGraphImgPath(BergDescription graph)
        {
            string dot = GenerateDotText(graph);
            return CreateFile(dot);
        }

        private static string CreateFile(string dot)
        {
            var totalPath = Path.Combine(PATH, FILE_NAME);
            File.WriteAllText(totalPath, dot);
            var args = string.Format(@"{0} -Tjpg -O", totalPath);
            StartHiddenProcess(@"C:\Program Files (x86)\Graphviz2.38\bin\dot.exe", args);
            return totalPath + ".jpg";
        }

        private static string GenerateDotText(BergDescription graph)
        {
            string dot = "", dotEdge;
            List<Edge> edges;
            if (graph is BergDirected)
            {
                dot += "digraph G { ";
                dotEdge = DOT_EDGE_ARROW;
                edges = graph.GetEdges();
            }
            else
            {
                dot += "graph G { ";
                dotEdge = DOT_EDGE_DEFAULT;
                edges = (graph as BergUndirected).GetDistinctEdges();
            }

            for (int i = 0; i < graph.FullArrayCount(); i++)
            {
                if (graph.IsExistsVertex(i))
                    dot += graph.FindVertexLabel(i) + ";";
            }

            for (int i = 0; i < edges.Count; i++)
            {
                var inputLabel = graph.FindVertexLabel(edges[i].InputIndex);
                var outputLabel = graph.FindVertexLabel(edges[i].OutputIndex);
                dot += String.Format("{0} {1} {2} [label=\"{3}\",weight=\"{3}\"]; ", inputLabel, dotEdge, outputLabel, edges[i].Value);
            }

            dot += "}";
            return dot;
        }

        private static void StartHiddenProcess(string path, string args)
        {
            var pInfo = new ProcessStartInfo()
            {
                FileName = path,
                Arguments = args,
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = false,
                RedirectStandardInput = true,
                RedirectStandardError = true
            };
            var process = Process.Start(pInfo);
            process.WaitForExit();
        }
    }
}
