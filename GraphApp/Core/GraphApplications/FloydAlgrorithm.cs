﻿using GraphApp.Core.GraphRepresentation;
using GraphApp.Core.GraphRepresentation.Berg;
using GraphApp.Core.Handlers;
using System;
using System.Diagnostics;

namespace GraphApp.Core.GraphApplications
{
    public class FloydAlgrorithm
    {
        public bool IsNeedRecalculate { get; set; }

        private double[,] _distanceMatrix;
        private BergDescription _graph;
        private double _radius;
        private double _diameter;

        public FloydAlgrorithm(BergDescription graph)
        {
            _graph = graph;
            IsNeedRecalculate = true;
        }

        public double Radius()
        {
            Execute();
            return _radius;
        }

        public double Diameter()
        {
            Execute();
            return _diameter;
        }

        private void Execute()
        {
            if (!IsNeedRecalculate)
                return;
            if (_graph.VertexCount() == 0 || _graph.EdgesCount == 0)
            {
                _diameter = 0;
                _radius = 0;
                IsNeedRecalculate = false;
                return;
            }
            CaluclateDistanceMatrix();
            CalculateValues();
        }

        private void CaluclateDistanceMatrix()
        {
            var n = _graph.FullArrayCount();
            var distanceMatrix = new double[n, n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (!_graph.IsExistsVertex(i) && !_graph.IsExistsVertex(j))
                        continue;
                    var edge = _graph.GetEdge(i, j);
                    if (i == j && edge == null)
                        distanceMatrix[i, j] = 0;
                    else
                        distanceMatrix[i, j] = edge?.Value ?? Double.MaxValue;
                }
            }

            for (int k = 0; k < n; k++)
                for (int j = 0; j < n; j++)
                    for (int i = 0; i < n; i++)
                    {
                        if (!_graph.IsExistsVertex(i) || !_graph.IsExistsVertex(j) || !_graph.IsExistsVertex(k))
                            continue;
                        distanceMatrix[i, j] = Math.Min(distanceMatrix[i, j], distanceMatrix[i, k] + distanceMatrix[k, j]);
                    }

            IsNeedRecalculate = false;
            _distanceMatrix = distanceMatrix;
        }

        private void CalculateValues()
        {
            int n = _graph.FullArrayCount();
            double[] eccentricity = new double[n];

            for (int i = 0; i < n; i++)
            {
                if (!_graph.IsExistsVertex(i))
                    continue;
                double maxInRowValue = double.MinValue;
                for (int j = 0; j < n; j++)
                {
                    if (!_graph.IsExistsVertex(j))
                        continue;
                    maxInRowValue = Math.Max(maxInRowValue, _distanceMatrix[i, j]);
                }
                eccentricity[i] = maxInRowValue;
            }

            _radius = Double.MaxValue;
            _diameter = Double.MinValue;

            for (int i = 0; i < n; i++)
            {
                if (!_graph.IsExistsVertex(i))
                    continue;
                _radius = eccentricity[i] < _radius ? eccentricity[i] : _radius;
                _diameter = eccentricity[i] > _diameter ? eccentricity[i] : _diameter;
            }
        }

        public double[,] GetDistanceMatrix()
        {
            Execute();
            return _distanceMatrix.Clone() as double[,];
        }
    }
}
