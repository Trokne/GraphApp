﻿using GraphApp.Core.GraphRepresentation;
using GraphApp.Core.GraphRepresentation.Berg;
using GraphApp.Core.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphApplications
{
    public static class GraphImporter
    {
        public static BergDirected ImportAdjacencyMatrix(string[] lines)
        {
            var n = Convert.ToInt32(lines[0]);
            string[] namesArray = new string[n];
            Array.Copy(lines, 1, namesArray, 0, n);
            namesArray = namesArray.Select(name => name.Replace("\"", "")).ToArray();
            var edgeArray = new Edge[n, n];
            var edgeCountArray = new int[n];
            for (int i = n + 1; i < lines.Length; i++)
            {
                var values = lines[i].Split(' ').Select(x => Convert.ToDouble(x)).ToArray();
                int edgeLastIndex = -1;
                for (int j = 0; j < values.Length; j++)
                {
                    if (values[j] == 0) continue;
                    edgeArray[i - n - 1, ++edgeLastIndex] = new Edge(i - n - 1, j, values[j]);
                }
                edgeCountArray[i - n - 1] = ++edgeLastIndex;
            }
            return new BergDirected(namesArray, edgeArray, edgeCountArray);
        }
        public static BergDirected ImportIncidenceMatrix(string[] lines)
        {
            var lengths = lines[0].Split(' ');
            var n = Convert.ToInt32(lengths[0]);
            var m = Convert.ToInt32(lengths[1]);
            string[] namesArray = new string[n];
            Array.Copy(lines, 1, namesArray, 0, n);
            namesArray = namesArray.Select(name => name.Replace("\"", "")).ToArray();
            var incidenceArray = new int[n, m];
            var edgeArray = new Edge[n, n];
            var edgeCountArray = new int[n];
            for (int i = 0; i < n; i++)
            {
                var values = lines[i + n + 1].Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
                for (int j = 0; j < m; j++)
                    incidenceArray[i, j] = values[j];
            }
            
            for (int i = 0; i < m; i++)
            {
                int inputIndex = -2, outputIndex = -2;
                for (int j = 0; j < n; j++)
                {
                    if (incidenceArray[j, i] == 1)
                        inputIndex = j;
                    else if (incidenceArray[j, i] == -1)
                        outputIndex = j;
                }
                if (inputIndex == -2 || outputIndex == -2)
                    continue;
                var edge = new Edge(inputIndex, outputIndex, 1);
                edgeArray[inputIndex, edgeCountArray[inputIndex]++] = edge;
            }

            return new BergDirected(namesArray, edgeArray, edgeCountArray);
        }
        public static BergDirected ImportArcsList(string[] lines)
        {
            var lengths = lines[0].Split(' ');
            var n = Convert.ToInt32(lengths[0]);
            var m = Convert.ToInt32(lengths[1]);
            string[] namesArray = new string[n];
            Array.Copy(lines, 1, namesArray, 0, n);
            namesArray = namesArray.Select(name => name.Replace("\"", "")).ToArray();
            var arcsArray = new double[3, m];
            var edgeArray = new Edge[n, n];
            var edgeCountArray = new int[n];
            for (int i = 0; i < 3; i++)
            {
                var values = lines[i + n + 1].Split(' ').Select(x => Convert.ToDouble(x)).ToArray();
                for (int j = 0; j < m; j++)
                    arcsArray[i, j] = values[j];
            }

            for (int i = 0; i < m; i++)
            {
                int input = Convert.ToInt32(arcsArray[0, i]) - 1;
                int output = Convert.ToInt32(arcsArray[1, i]) - 1;
                double value = arcsArray[2, i];
                var edge = new Edge(input, output, value);
                edgeArray[input, edgeCountArray[input]++] = edge;
            }

            return new BergDirected(namesArray, edgeArray, edgeCountArray);
        }
        public static BergDirected ImportBergDescription(string[] lines)
        {
            var splittedLines = lines.Select(line => Regex.Split(line, StringHelper.SplitRegex)).ToArray();
            int vertexCount = splittedLines.Count();
            var namesArray = splittedLines.Select(line => line[0].Replace("\"\"", "")).ToArray();
            var edgesArray = new Edge[vertexCount, vertexCount];
            var edgesCountArray = new int[vertexCount];
            for (int i = 0; i < vertexCount; i++)
            {
                Edge edge = null;
                int arrayIndex = -1;
                for (int j = 1; j < splittedLines[i].Count(); j++)
                {
                    string fileValue = splittedLines[i][j];
                    if (fileValue.Contains("\""))
                    {
                        var indexInNames = Array.FindIndex(namesArray, n => n == fileValue);
                        if (indexInNames == -1) continue;
                        edge = new Edge(i, indexInNames, 0);
                        edgesArray[i, ++arrayIndex] = edge;
                    }
                    else
                    {
                        if (edge == null) continue;
                        edgesArray[i, arrayIndex].Value = Convert.ToDouble(fileValue);
                    }
                }
                edgesCountArray[i] = arrayIndex + 1;

            }
            for (int i = 0; i < namesArray.Length; i++)
                namesArray[i] = namesArray[i].Replace("\"", "");
            var graph = new BergDirected(namesArray, edgesArray, edgesCountArray);
            return graph;
        }

    }
}
