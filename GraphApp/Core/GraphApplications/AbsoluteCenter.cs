﻿using GraphApp.Core.GraphRepresentation.Berg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphApplications
{
    public class AbsoluteCenter
    {
        private BergUndirected _graph;

        public AbsoluteCenter(BergUndirected graph)
        {
            _graph = graph;
        }

        public string GetAbsoluteCenter()
        {
            List<string> outputText = new List<string>();
            if (_graph.ConnectivityComponents() != 1)
                return "Ошибка! Граф не связный.";
            var distanceMatrix = _graph.GetDistanceMatrix();
            double minmax = int.MaxValue;
            int r;
            var vertexAndRad = new Dictionary<string, double>();
            for (int i = 0; i < _graph.FullArrayCount(); i++)
            {
                if (!_graph.IsExistsVertex(i)) continue;
                double max = double.MinValue;
                for (int j = 1; j < _graph.FullArrayCount(); j++)
                {
                    if (!_graph.IsExistsVertex(j)) continue;
                    if (distanceMatrix[i, j] > max)
                        max = distanceMatrix[i, j];
                }
                vertexAndRad.Add(_graph.FindVertexLabel(i), max);
                if (minmax > max)
                {
                    r = i;
                    minmax = max;
                }
            }
            double centerRad = minmax;
            var vertices = new List<string>();
            foreach (var elem in vertexAndRad.Keys)
                if (vertexAndRad[elem] == minmax) vertices.Add(elem);
            double di = 0, dj = 0;
            for (int i = 0; i < _graph.FullArrayCount() - 1; i++)
            {
                if (!_graph.IsExistsVertex(i)) continue;
                for (int j = i + 1; j < _graph.FullArrayCount(); j++)
                {
                    if (!_graph.IsExistsVertex(j)) continue;
                    var edge = _graph.GetEdge(i, j);
                    if (edge == null) continue;
                    double MaxI = 0; //расстояние от i до самой удаленной вершины
                    double MaxJ = 0; //расстояние от j до самой удаленной вершины
                    for (int k = 0; k < _graph.FullArrayCount(); k++)
                    {
                        if (!_graph.IsExistsVertex(k)) continue;
                        if (k != i && k != j)
                        {
                            if (distanceMatrix[i, k] < 10000)
                                di = distanceMatrix[i, k];
                            if (distanceMatrix[j, k] < 10000)
                                dj = distanceMatrix[j, k];
                            if (di > dj)
                            {
                                if (dj > MaxJ)
                                    MaxJ = dj;
                            }
                            else
                            {
                                if (di > MaxI)
                                    MaxI = di;
                            }
                        }
                        var weight = edge.Value;
                        double dist = (MaxI + weight + MaxJ) / 2; //Расстояние между внутренними точками дуги i-j и максимально удаленной от них вершиной равно
                        var x = dist - MaxI;
                        if (dist <= minmax && x > 0 && weight - x > 0) //мягкое условие. Если мягкое условие, то может найти абс. центр с радиусом = радиусу центра в вершине.
                        {
                            if (dist < minmax) outputText.Clear();
                            minmax = dist;
                            outputText.Add("Ребро " + _graph.FindVertexLabel(i) + "-" + _graph.FindVertexLabel(j) + ";\n" +
                                "удаление точки от вершины " + _graph.FindVertexLabel(i) + ": " + x.ToString() + ";\n" +
                                "радиус " + minmax.ToString());
                        }
                    }
                }
            }

            if (outputText.Count() == 0)
            {
                string s = "";
                foreach (var elem in vertices)
                    s += elem + ", ";
                s = s.Remove(s.Length - 2);
                return $"АЦ находится в вершинах: {s}." +
                    $"\nРадиус: {minmax}";
            }
            else
            {
                string s = "";
                foreach (var elem in outputText)
                    s += elem + "\n";
                if (minmax == centerRad)
                {
                    s += "АЦ находится в вершинах:\n";
                    foreach (var elem in vertices)
                        s += elem + ", ";
                    s = s.Remove(s.Length - 2);
                    s += $"\nРадиус = {minmax}\n";
                }
                return $"Абсолютный центр графа: \n{s}";
            }
        }
    }
}
