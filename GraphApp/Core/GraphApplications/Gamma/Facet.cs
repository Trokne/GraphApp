﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphApplications.Gamma
{
    public class Facet
    {
        public List<string> Verticies { get; set; }
        public bool IsInner { get; set; }

        public Facet()
        {
            Verticies = new List<string>();
            IsInner = true;
        }

        public Facet(List<string> indicies, bool isInner)
        {
            Verticies = new List<string>(indicies);
            IsInner = isInner;
        }
    }
}
