﻿using GraphApp.Core.GraphRepresentation.Berg;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Core.GraphApplications.Gamma
{
    public class GammaAlgorithm
    {
        private BergUndirected _sourceGraph;
        private BergUndirected _planeGraph;
        private List<BergUndirected> _segments;
        private List<Facet> _facets;

        public GammaAlgorithm(BergUndirected graph)
        {
            _sourceGraph = graph;
            _facets = new List<Facet>();
        }

        public bool IsPlanar()
        {
            InitializeAlgorithm();
            bool isPlanar = true;
            do
            {
                BuildSegments();
                CalculateConects(out List<List<string>> conectVerticies);
                foreach (var conect in conectVerticies)
                {
                    if (conect.Count < 2)
                        isPlanar = false;
                }
                CalculateFacets(out Dictionary<int, List<Facet>> facets, conectVerticies);
                var minSegmentIndex = GetMinCountIndex(facets);
                if (minSegmentIndex == -1)
                    isPlanar = false;
                else if (!isPlanar)
                {
                    var segment = _segments[minSegmentIndex];
                    var facet = facets[minSegmentIndex][0];
                    var chain = GetChain(segment, facet);
                    AddChainToPlaneGraph(chain, facet);
                }

            } while (_planeGraph.EdgesCount != _sourceGraph.EdgesCount 
                     && isPlanar);
            return isPlanar;
        }

        private void AddChainToPlaneGraph(List<string> chain, Facet facet)
        {
            for (int i = 0; i < chain.Count; i++)
                _planeGraph.AddVertex(chain[i]);
            for (int i = 0; i < chain.Count - 1; i++)
                _planeGraph.AddEdge(chain[i], chain[i + 1]);
            var firstFacet = new Facet();
            var secondFacet = new Facet();
            firstFacet.IsInner = secondFacet.IsInner = facet.IsInner;
            // Первый случай - две контактные вершины
            if (chain.Count == 2)
            {
                bool isStarted = false;
                for (int i = 0; i < facet.Verticies.Count; i++)
                {
                    if (!isStarted && facet.Verticies[i] == chain[0])
                    {
                        secondFacet.Verticies.Add(facet.Verticies[i]);
                        isStarted = true;
                    }
                    if (isStarted)
                        firstFacet.Verticies.Add(facet.Verticies[i]);
                    else secondFacet.Verticies.Add(facet.Verticies[i]);
                    if (isStarted && facet.Verticies[i] == chain[chain.Count - 1])
                    {
                        isStarted = false;
                        secondFacet.Verticies.Add(facet.Verticies[i]);
                    }
                }
            }
            // Второй случай - присутствуют не только контактные вершины
            else
            {
                bool isStarted = false;
                for (int i = 0; i < facet.Verticies.Count; i++)
                {
                    if (!isStarted)
                        firstFacet.Verticies.Add(facet.Verticies[i]);
                    if (facet.Verticies[i] == chain[0])
                    {
                        for (int j = 1; j < chain.Count; j++)
                            firstFacet.Verticies.Add(chain[j]);
                        isStarted = true;
                    }
                    if (facet.Verticies[i] == chain[chain.Count - 1])
                        isStarted = false;
                }
                isStarted = false;
                for (int i = 0; i < facet.Verticies.Count; i++)
                {
                    if (isStarted)
                        secondFacet.Verticies.Add(facet.Verticies[i]);
                    if (facet.Verticies[i] == chain[0])
                    {
                        secondFacet.Verticies.Add(chain[0]);
                        isStarted = true;
                    }
                    else if (facet.Verticies[i] == chain[chain.Count - 1])
                    {
                        isStarted = false;
                        for (int j = 1; j < chain.Count - 1; j++)
                            secondFacet.Verticies.Add(chain[j]);
                    }                    
                }
            }
            _facets.Remove(facet);
            _facets.Add(firstFacet);
            _facets.Add(secondFacet);
        }

        private List<string> GetChain(BergUndirected segment, Facet facet)
        {
            List<string> chain = new List<string>();
            for (int i = 0; i < segment.FullArrayCount(); i++)
            {
                if (!segment.IsExistsVertex(i)) continue;
                var label = segment.FindVertexLabel(i);
                if (_planeGraph.IsExistsVertex(label))
                {
                    chain.Add(label);
                    break;
                }
            }
            string curLabel, lastLabel = "";
            do
            {
                curLabel = chain[chain.Count - 1];
                string findLabel;
                foreach (var edge in segment.GetEdges(curLabel))
                {
                    findLabel = segment.FindVertexLabel(edge.OutputIndex);
                    if (findLabel != lastLabel)
                    {
                        chain.Add(findLabel);
                        break;
                    }
                }
                lastLabel = curLabel;
            } while (!InPlaneGraph(chain[chain.Count - 1]));
            return chain;
        }

        private bool InPlaneGraph(string label)
        {
            return _planeGraph.IsExistsVertex(label);
        }

        private int GetMinCountIndex(Dictionary<int, List<Facet>> facets)
        {
            int minCount = int.MaxValue, index = -1;
            foreach (var facet in facets)
            {
                var count = facet.Value.Count();
                if (count == 0) return -1;
                if (count < minCount)
                {
                    minCount = count;
                    index = facet.Key;
                }
            }
            return index;
        }

        private void CalculateFacets(out Dictionary<int, List<Facet>> facetSets, List<List<string>> conectVerticies)
        {
            facetSets = new Dictionary<int, List<Facet>>();
            foreach (var facet in _facets)
            {
                for (int i = 0; i < conectVerticies.Count; i++)
                {
                    if (!facetSets.ContainsKey(i))
                        facetSets[i] = new List<Facet>();
                    bool isContainAll = true;
                    foreach (var vertex in conectVerticies[i])
                    {
                        if (!facet.Verticies.Contains(vertex))
                        {
                            isContainAll = false;
                            break;
                        }
                    }
                    if (isContainAll)
                        facetSets[i].Add(facet);
                }
            }
        }

        private void CalculateConects(out List<List<string>> conectVerticies)
        {
            conectVerticies = new List<List<string>>(_segments.Count);
            for (int i = 0; i < _segments.Count; i++)
            {
                conectVerticies.Add(new List<string>());
                for (int j = 0; j < _segments[i].FullArrayCount(); j++)
                {
                    if (!_segments[i].IsExistsVertex(j)) continue;
                    var label = _segments[i].FindVertexLabel(j);
                    if (_planeGraph.IsExistsVertex(label))
                        conectVerticies[i].Add(label);
                }
            }
        }

        private void BuildSegments()
        {
            _segments = new List<BergUndirected>();
            AddOneEdgeSegments();
            AddMultipleSegments();
        }

        private void AddOneEdgeSegments()
        {
            for (int i = 0; i < _planeGraph.FullArrayCount(); i++)
            {
                if (!_planeGraph.IsExistsVertex(i)) continue;
                var firstLabel = _planeGraph.FindVertexLabel(i);
                for (int j = 0; j < _planeGraph.FullArrayCount(); j++)
                {
                    if (i == j || !_planeGraph.IsExistsVertex(j))
                        continue;
                    var secondLabel = _planeGraph.FindVertexLabel(j);
                    if (_planeGraph.GetEdge(firstLabel, secondLabel) != null)
                        continue;
                    var edge = _sourceGraph.GetEdge(firstLabel, secondLabel);
                    if (edge == null) continue;
                    bool isContainEdge = false;
                    foreach (var segment in _segments)
                    {
                        var segmentEdge = segment.GetEdge(firstLabel, secondLabel);
                        if (segmentEdge != null)
                        {
                            isContainEdge = true;
                            break;
                        }
                    }
                    if (!isContainEdge)
                    {
                        BergUndirected segment = new BergUndirected();
                        segment.AddVertex(firstLabel);
                        segment.AddVertex(secondLabel);
                        segment.AddEdge(firstLabel, secondLabel);
                        _segments.Add(segment);
                    }
                }
            }
        }

        private void AddMultipleSegments()
        {
            for (int i = 0; i < _sourceGraph.FullArrayCount(); i++)
            {
                if (!_sourceGraph.IsExistsVertex(i)) continue;
                var label = _sourceGraph.FindVertexLabel(i);
                if (_planeGraph.IsExistsVertex(label)
                    || _segments.Any(s => s.IsExistsVertex(label)))
                    continue;
                BergUndirected segment = new BergUndirected();
                FillSegmentEdges(segment, label, new HashSet<string>());
                _segments.Add(segment);
            }
        }

        private void FillSegmentEdges(BergUndirected segment, string inputLabel, HashSet<string> from)
        {
            segment.AddVertex(inputLabel);
            foreach (var edge in _sourceGraph.GetEdges(inputLabel))
            {
                string outputLabel = _sourceGraph.FindVertexLabel(edge.OutputIndex);
                if (from.Contains(outputLabel)) continue;
                segment.AddVertex(outputLabel);
                segment.AddEdge(inputLabel, outputLabel);
                if (!_planeGraph.IsExistsVertex(outputLabel))
                {
                    from.Add(inputLabel);
                    FillSegmentEdges(segment, outputLabel, from);
                }
            }
        }

        private void InitializeAlgorithm()
        {
            _planeGraph = new BergUndirected();
            List<int> cycle = _sourceGraph.GetCycle();
            Facet innerFacet = new Facet();
            Facet outerFacet = new Facet();
            foreach (var vertex in cycle)
            {
                var label = _sourceGraph.FindVertexLabel(vertex);
                _planeGraph.AddVertex(label);
                innerFacet.Verticies.Add(label);
                outerFacet.Verticies.Add(label);
            }
            innerFacet.IsInner = true;
            outerFacet.IsInner = false;
            _facets.Add(innerFacet);
            _facets.Add(outerFacet);
            for (int i = 0; i < cycle.Count - 1; i++)
            {
                string label1 = _sourceGraph.FindVertexLabel(cycle[i]);
                string label2 = _sourceGraph.FindVertexLabel(cycle[i + 1]);
                _planeGraph.AddEdge(label1, label2);
                if (i == 0)
                {
                    string endLabel = _sourceGraph.FindVertexLabel(cycle[cycle.Count - 1]);
                    _planeGraph.AddEdge(label1, endLabel);
                }   
            }
        }
    }
}
