﻿using GraphApp.Core.GraphRepresentation;
using System.Collections.Generic;
using System.Linq;
using System;
using GraphApp.Core.GraphRepresentation.Berg;

namespace GraphApp.Core.GraphApplications
{
    public class DephFirstSearch
    {
        private enum VertexColor { White, Gray, Black }
        private VertexColor[] _colors;
        private Queue<string> _vertexQueue;
        private BergUndirected _graph;
        private int _connectivityComponents;
        private bool _isContainCycle;
        private bool _isTree;
        private List<int> _cycleVerticies;

        public bool IsNeedExecute { get; set; }

        public DephFirstSearch(BergUndirected graph)
        {
            _graph = graph;
            InitializeByDefaultValues();
            IsNeedExecute = true;
        }

        private void InitializeByDefaultValues()
        {
            _isContainCycle = false;
            _isTree = false;
            _connectivityComponents = 0;
        }

        public int ConnectivityComponents()
        {
            Execute();
            return _connectivityComponents;
        }

        public bool IsContainCycle()
        {
            Execute();
            return _isContainCycle;
        }

        public bool IsTree()
        {
            Execute();
            return _isTree;
        }

        public List<int> GetCycle()
        {
            Execute();
            if (!_isContainCycle) return null;
            return new List<int>(_cycleVerticies);
        }

        private Queue<string> Execute()
        {
            if (!IsNeedExecute)
                return null;
            if (_graph.VertexCount() == 0)
            {
                InitializeByDefaultValues();
                IsNeedExecute = false;
                return null;
            }
            var startIndex = _graph.FindRootIndex();
            if (startIndex == -1) startIndex = 0;
            return Execute(startIndex);
        }

        public Queue<string> Execute(string startVertex)
        {
            int startIndex = _graph.FindVertex(startVertex);
            return Execute(startIndex);
        }

        public Queue<string> Execute(int startIndex)
        {
            if (startIndex < 0) return null;
            var range = Enumerable.Range(startIndex, _graph.FullArrayCount());
            if (startIndex >= 0)
            {
                var secondRange = Enumerable.Range(0, startIndex);
                range = range.Union(secondRange);
            }
            InitializeByDefaultValues();
            _colors = new VertexColor[_graph.FullArrayCount()];
            _vertexQueue = new Queue<string>();
            foreach (var i in range)
            {
                if (_graph.IsExistsVertex(i) &&
                    _colors[i] == VertexColor.White)
                {
                    _connectivityComponents++;
                    ExecuteDFS(i, new List<int>());
                }
            }
            _isTree = !_isContainCycle &&
                     _colors.Count(x => x == VertexColor.Black) == _graph.VertexCount() &&
                     _connectivityComponents == 1 &&
                     _graph.VertexCount() - _graph.EdgesCount == 1;
            IsNeedExecute = false;
            return _vertexQueue;
        }

        private void ExecuteDFS(int i, List<int> verticies, int from = -1)
        {
            verticies = new List<int>(verticies) { i };
            if (_colors[i] == VertexColor.Gray)
            {
                _isContainCycle = true;
                if (_cycleVerticies == null || _cycleVerticies.Count == 0)
                {
                    _cycleVerticies = new List<int>(verticies);
                    _cycleVerticies.RemoveAt(_cycleVerticies.Count - 1);
                }
            }
            if (_colors[i] != VertexColor.White)
                return;
            _vertexQueue.Enqueue(_graph.FindVertexLabel(i));
            _colors[i] = VertexColor.Gray;
            foreach (var edge in _graph.GetEdges(i))
            {
                if (edge.OutputIndex != from)
                    ExecuteDFS(edge.OutputIndex, verticies, i);
            }
            _colors[i] = VertexColor.Black;
        }
    }
}
