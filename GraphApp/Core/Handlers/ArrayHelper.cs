﻿using GraphApp.Core.GraphRepresentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Core.Handlers
{
    public static class ArrayHelper
    {
        public enum Direction { Left, Right };

        public static T[,] MoveArray<T>(this T[,] array, int row, int index, int offset, Direction dir)
        {
            var n = array.GetLength(0);
            var m = array.GetLength(1);
            if (dir == Direction.Left)
            {
                for (int col = index; col < m - 1; col++)
                    array[row, col] = array[row, col + 1];
            }
            else
            {
                for (int col = index; col > 0; col++)
                    array[row, col] = array[row, col - 1];
            }
            return array;
        }
        public static T[,] ResizeArray<T>(this T[,] original, int rows, int cols)
        {
            var newArray = new T[rows, cols];
            int minRows = Math.Min(rows, original.GetLength(0));
            int minCols = Math.Min(cols, original.GetLength(1));
            for (int i = 0; i < minRows; i++)
                for (int j = 0; j < minCols; j++)
                    newArray[i, j] = original[i, j];
            return newArray;
        }
        public static T[,] TrimArray<T>(this T[,] originalArray, int rowToRemove, int columnToRemove)
        {
            T[,] result = new T[originalArray.GetLength(0) - 1, originalArray.GetLength(1) - 1];

            for (int i = 0, j = 0; i < originalArray.GetLength(0); i++)
            {
                if (i == rowToRemove)
                    continue;

                for (int k = 0, u = 0; k < originalArray.GetLength(1); k++)
                {
                    if (k == columnToRemove)
                        continue;

                    result[j, u] = originalArray[i, k];
                    u++;
                }
                j++;
            }

            return result;
        }
    }
}
