﻿using GraphApp.Core.GraphApplications;
using GraphApp.Core.GraphRepresentation;
using GraphApp.Core.GraphRepresentation.Berg;
using GraphApp.Core.GraphRepresentation.Description;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GraphApp.Core.Handlers
{
    public static class FileHandler
    {
        public static string GetGraphFile()
        {
            var ofd = new OpenFileDialog
            {
                Filter = "Текстовые файлы(*.txt)|*.txt",
                CheckFileExists = true,
                Multiselect = false,
                CheckPathExists = true
            };
            if (ofd.ShowDialog() == true)
                return ofd.FileName;
            else return String.Empty;
        }

        public static void ImportFile(string fileName, GraphInformation.GraphType type)
        {
            string[] lines;
            try
            {
                lines = File.ReadAllLines(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
            BergDescription graph = null;
            switch (type)
            {
                case GraphInformation.GraphType.ArcsList:
                    graph = GraphImporter.ImportArcsList(lines);
                    break;
                case GraphInformation.GraphType.BergDescription:
                    graph = GraphImporter.ImportBergDescription(lines);
                    break;
                case GraphInformation.GraphType.AdjacencyMatrix:
                    graph = GraphImporter.ImportAdjacencyMatrix(lines);
                    break;
                case GraphInformation.GraphType.IncidenceMatrix:
                    graph = GraphImporter.ImportIncidenceMatrix(lines);
                    break;
            }
            if (graph != null) GraphInformation.Graph = graph;
        }
    }
}
