﻿using GraphApp.Core.GraphApplications;
using GraphApp.Core.GraphRepresentation.Berg;
using GraphApp.Core.GraphRepresentation.Description;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphApp.UI.Controls
{
    /// <summary>
    /// Логика взаимодействия для VertexOpsWindow.xaml
    /// </summary>
    public partial class VertexOpsWindow : UserControl
    {
        private Grid _owner;
        private int _operation;

        public VertexOpsWindow(int operation)
        {
            InitializeComponent();
            _operation = operation;
            InitializeLabels();
        }

        private void InitializeLabels()
        {
            switch (_operation)
            {
                case (int)GraphInformation.GeneralOperations.GraphType:
                    btnApply.Content = "Сменить ориентацию";
                    tbLabel.Visibility = Visibility.Collapsed;
                    lblDescription.Visibility = Visibility.Collapsed;
                    break;
                case (int)GraphInformation.GeneralOperations.AddVertex:
                    btnApply.Content = "Добавить";
                    break;
                case (int)GraphInformation.GeneralOperations.FindAdjVertices:
                case (int)GraphInformation.UndirectedOperations.VertexListByDFS:
                    btnApply.Content = "Найти";
                    break;
                case (int)GraphInformation.UndirectedOperations.PrintCycle:
                    tbLabel.Visibility = Visibility.Collapsed;
                    lblDescription.Visibility = Visibility.Collapsed;
                    btnApply.Content = "Вывести цикл";
                    break;
                case (int)GraphInformation.UndirectedOperations.IsPlanar:
                    tbLabel.Visibility = Visibility.Collapsed;
                    lblDescription.Visibility = Visibility.Collapsed;
                    btnApply.Content = "Показать";
                    break;
                case (int)GraphInformation.UndirectedOperations.AbsoluteCenter:
                    tbLabel.Visibility = Visibility.Collapsed;
                    lblDescription.Visibility = Visibility.Collapsed;
                    tbResult.TextWrapping = TextWrapping.Wrap;
                    btnApply.Content = "Найти";
                    break;
                case (int)GraphInformation.GeneralOperations.VertexList:
                    btnApply.Content = "Найти";
                    tbLabel.Visibility = Visibility.Collapsed;
                    lblDescription.Visibility = Visibility.Collapsed;
                    break;
                case (int)GraphInformation.GeneralOperations.RemoveVertex:
                    btnApply.Content = "Удалить";
                    break;
            }
        }

        public void Show(Grid owner)
        {
            _owner = owner;   
            _owner.Children.Add(this);
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            switch (_operation)
            {
                case (int)GraphInformation.GeneralOperations.AddVertex:
                    AddVertex();
                    ((MainWindow)Application.Current.MainWindow).DisplayInformation();
                    break;
                case (int)GraphInformation.GeneralOperations.FindAdjVertices:
                    FindVertexes();
                    break;
                case (int)GraphInformation.GeneralOperations.RemoveVertex:
                    RemoveVertex();
                    ((MainWindow)Application.Current.MainWindow).DisplayInformation();
                    break;
                case (int)GraphInformation.GeneralOperations.VertexList:
                    GetVertexList();
                    break;
                case (int)GraphInformation.GeneralOperations.GraphType:
                    SwitchGraphType();
                    ((MainWindow)Application.Current.MainWindow).DisplayInformation();
                    ((MainWindow)Application.Current.MainWindow).InitializeOperations();
                    break;
                case (int)GraphInformation.UndirectedOperations.VertexListByDFS:
                    GetVertexListByDFS();
                    break;
                case (int)GraphInformation.UndirectedOperations.PrintCycle:
                    GetCycle();
                    break;
                case (int)GraphInformation.UndirectedOperations.IsPlanar:
                    IsPlanarGraph();
                    break;
                case (int)GraphInformation.UndirectedOperations.AbsoluteCenter:
                    GetAbsoluteCenter();
                    break;
            }
            
        }

        private void GetAbsoluteCenter()
        {
            BergUndirected graph = GraphInformation.Graph as BergUndirected;
            tbResult.Text = graph.GetAbsoluteCenter();
        }

        private void IsPlanarGraph()
        {
            BergUndirected graph = GraphInformation.Graph as BergUndirected;
            if (graph.IsTree() || !graph.IsContainCycle() || graph.IsPlanar())
                tbResult.Text = "Граф планарен.";
            else tbResult.Text = "Граф не планарен.";
        }

        private void GetCycle()
        {
            BergUndirected graph = GraphInformation.Graph as BergUndirected;
            var cycle = graph.GetCycle();
            if (cycle == null || cycle.Count == 0)
            {
                tbResult.Text = "Циклов не найдено.";
                return;
            }
            StringBuilder str = new StringBuilder();
            foreach (var v in cycle)
            {
                var label = graph.FindVertexLabel(v);
                if (String.IsNullOrWhiteSpace(label)) continue;
                str.Append(label + ", ");
            }
            string resultText = str.ToString();
            if (!string.IsNullOrWhiteSpace(resultText))
            {
                resultText = resultText.Remove(resultText.Length - 2);
                tbLabel.Text = "";
            }
            else resultText = "Вершины отсутствуют";
            tbResult.Text = resultText;
        }

        private void SwitchGraphType()
        {
            tbResult.Text = "Граф стал ";
            if (GraphInformation.Graph is BergDirected)
            {
                GraphInformation.Graph = new BergUndirected(GraphInformation.Graph);
                tbResult.Text += "неориентированным.";
            }
            else
            {
                GraphInformation.Graph = new BergDirected(GraphInformation.Graph);
                tbResult.Text += "ориентированным.";
            }
            
        }

        private void GetVertexListByDFS()
        {
            var graph = GraphInformation.Graph;
            var dfs = new DephFirstSearch(graph as BergUndirected);
            if (!graph.IsExistsVertex(tbLabel.Text))
            {
                tbResult.Text = "Выбранная вершина отсутствует в графе";
                return;
            }
            var labels = dfs.Execute(tbLabel.Text);
            string resultLabel = "";
            foreach (var label in labels)
            {
                if (String.IsNullOrEmpty(label)) continue;
                resultLabel += label + ", " ?? "";
            }
            if (!string.IsNullOrWhiteSpace(resultLabel))
            {
                resultLabel = resultLabel.Remove(resultLabel.Length - 2);
                tbLabel.Text = "";
            }
            else resultLabel = "Вершины отсутствуют";
            tbResult.Text = resultLabel;
        }

        private void GetVertexList()
        {
            var graph = GraphInformation.Graph;
            string resultLabel = "";
            for (int i = 0; i < graph.FullArrayCount(); i++)
            {
                string label = graph.FindVertexLabel(i);
                if (String.IsNullOrEmpty(label)) continue;
                resultLabel += label + ", " ?? "";
            }
            if (!string.IsNullOrWhiteSpace(resultLabel))
            {
                resultLabel = resultLabel.Remove(resultLabel.Length - 2);
                tbLabel.Text = "";
            }
            else resultLabel = "Вершины отсутствуют";
            tbResult.Text = resultLabel;
        }

        private void RemoveVertex()
        {
            if (String.IsNullOrWhiteSpace(tbLabel.Text))
            {
                tbResult.Text = "Для удаления вершины введите её строковую метку";
                return;
            }
            var isSuccess = GraphInformation.Graph.RemoveVertex(tbLabel.Text);
            tbResult.Text = isSuccess ? $"Вершина {tbLabel.Text} успешно удалена" : "Ошибка! Указанной веришны не существует";
            if (isSuccess) tbLabel.Text = "";
        }

        private void FindVertexes()
        {
            var label = tbLabel.Text;
            if (String.IsNullOrWhiteSpace(label))
            {
                tbResult.Text = "Для поиска вершины введите её строковую метку";
                return;
            }
            try
            {
                string resultLabel = "";
                foreach (var vertex in GraphInformation.Graph.GetEdges(label))
                    resultLabel += GraphInformation.Graph.FindVertexLabel(vertex.OutputIndex) + ", ";
                if (!string.IsNullOrWhiteSpace(resultLabel))
                {
                    resultLabel = resultLabel.Remove(resultLabel.Length - 2);
                    tbLabel.Text = "";
                }
                else resultLabel = "Смежные вершины отсутствуют";
                tbResult.Text = resultLabel;
            }
            catch
            {
                tbResult.Text = "Вершина с данной меткой не найдена!";
            }
        }

        private void AddVertex()
        {
            if (String.IsNullOrWhiteSpace(tbLabel.Text))
            {
                tbResult.Text = "Для добавления вершины введите её строковую метку";
                return;
            }
            var isSuccess = GraphInformation.Graph.AddVertex(tbLabel.Text);
            tbResult.Text = isSuccess ? $"Вершина {tbLabel.Text} успешно добавлена" : "Ошибка! Нельзя добавить существующую вершину";
            if (isSuccess) tbLabel.Text = "";
        }
    }
}
