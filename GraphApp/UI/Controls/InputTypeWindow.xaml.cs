﻿using GraphApp.Core.GraphRepresentation.Description;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GraphApp.Core.Handlers;
using GraphApp.Core;

namespace GraphApp.UI.Controls
{
    /// <summary>
    /// Логика взаимодействия для InputTypeWindow.xaml
    /// </summary>
    public partial class InputTypeWindow : UserControl
    {
        private Grid _owner;
        private Grid[] _elementsToHide;
        private string _fileName;

        public InputTypeWindow(string fileName)
        {
            InitializeComponent();
            InitializeCombobox();
            _fileName = fileName;
        }

        private void InitializeCombobox()
        {            
            var types = EnumHelper.GetAttributeDescriptions<GraphInformation.GraphType>();
            foreach (var type in types)
                cbFileType.Items.Add(type);
        }

        public void Show(Grid owner, params Grid[] elementsToHide)
        {
            _owner = owner;
            _elementsToHide = elementsToHide;
            owner.Children.Add(this);
            foreach(var elem in elementsToHide)
            {
                elem.IsEnabled = false;
                elem.Opacity = 0.6;
            }
        }

        private void Close()
        {
            foreach (var elem in _elementsToHide)
            {
                elem.IsEnabled = true;
                elem.Opacity = 1;
            }
            _owner.Children.Remove(this);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnImportGraph_Click(object sender, RoutedEventArgs e)
        {
            if (Save())
            {
                Close();
                ((MainWindow)Application.Current.MainWindow).DisplayInformation();
            }
        }

        private bool Save()
        {
            if (cbFileType.SelectedIndex == -1)
                return false;
            FileHandler.ImportFile(_fileName, (GraphInformation.GraphType)cbFileType.SelectedIndex);
            return true;
        }
    }
}
