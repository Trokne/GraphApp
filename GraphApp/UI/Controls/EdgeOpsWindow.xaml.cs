﻿using GraphApp.Core.GraphRepresentation.Description;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphApp.UI.Controls
{
    /// <summary>
    /// Логика взаимодействия для EdgeOpsWindow.xaml
    /// </summary>
    public partial class EdgeOpsWindow : UserControl
    {
        private Grid _owner;
        GraphInformation.GeneralOperations _operation;

        public EdgeOpsWindow(GraphInformation.GeneralOperations operation)
        {
            InitializeComponent();
            _operation = operation;
            InitializeLabels();
        }

        private void InitializeLabels()
        {
            switch (_operation)
            {
                case GraphInformation.GeneralOperations.AddEdge:
                    btnApply.Content = "Добавить";
                    break;
                case GraphInformation.GeneralOperations.FindEdge:
                    lblValue.Visibility = Visibility.Collapsed;
                    tbValue.Visibility = Visibility.Collapsed;
                    btnApply.Content = "Найти";
                    break;
                case GraphInformation.GeneralOperations.RemoveEdge:
                    btnApply.Content = "Удалить";
                    lblValue.Visibility = Visibility.Collapsed;
                    tbValue.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void ClearInputs()
        {
            tbLabel1.Clear();
            tbLabel2.Clear();
            tbValue.Clear();
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            switch (_operation)
            {
                case GraphInformation.GeneralOperations.AddEdge:
                    AddEdge();
                    ((MainWindow)Application.Current.MainWindow).DisplayInformation();
                    break;
                case GraphInformation.GeneralOperations.FindEdge:
                    FindEdge();
                    break;
                case GraphInformation.GeneralOperations.RemoveEdge:
                    RemoveEdge();
                    ((MainWindow)Application.Current.MainWindow).DisplayInformation();
                    break;
            }
        }

        private void RemoveEdge()
        {
            var vertex1 = tbLabel1.Text;
            var vertex2 = tbLabel2.Text;
            if (String.IsNullOrWhiteSpace(vertex1) ||
                String.IsNullOrWhiteSpace(vertex2))
            {
                tbResult.Text = "Ошибка! Необходимо заполнить все поля";
                return;
            }
            bool isSuccess = GraphInformation.Graph.RemoveEdge(vertex1, vertex2);
            if (isSuccess)
            {
                ClearInputs();
                tbResult.Text = $"Ребро о {vertex1} к {vertex2} успешно удалено";
            }
            else tbResult.Text = "Ошибка! Необходимо ввести корректные вершины";
        }

        private void FindEdge()
        {
            var vertex1 = tbLabel1.Text;
            var vertex2 = tbLabel2.Text;
            if (String.IsNullOrWhiteSpace(vertex1) ||
                String.IsNullOrWhiteSpace(vertex2))
            {
                tbResult.Text = "Ошибка! Необходимо заполнить все поля";
                return;
            }
            var edge = GraphInformation.Graph.GetEdge(vertex1, vertex2);
            if (edge == null)
                tbResult.Text = "Ребро не найдено";
            else
            {
                tbResult.Text = $"Значение ребра [{vertex1}, {vertex2}]: {edge.Value}";
                ClearInputs();
            }
        }

        private void AddEdge()
        {
            var vertex1 = tbLabel1.Text;
            var vertex2 = tbLabel2.Text;
            var valueText = tbValue.Text;
            if (String.IsNullOrWhiteSpace(vertex1) ||
                String.IsNullOrWhiteSpace(vertex2) ||
                String.IsNullOrWhiteSpace(valueText))
            {
                tbResult.Text = "Ошибка! Необходимо заполнить все поля";
                return;
            }
            if (!Double.TryParse(valueText, out double value))
            {
                tbResult.Text = "Ошибка! Поле значения должно быть корректным";
                return;
            }
            bool isSuccess = GraphInformation.Graph.AddEdge(vertex1, vertex2, Convert.ToDouble(valueText));
            if (isSuccess)
            {
                ClearInputs();
                tbResult.Text = $"Ребро от {vertex1} к {vertex2} успешно добавлено";
            }
            else tbResult.Text = "Ошибка! Необходимо ввести корректные вершины";
        }

        public void Show(Grid owner)
        {
            _owner = owner;
            _owner.Children.Add(this);
        }

    }
}
