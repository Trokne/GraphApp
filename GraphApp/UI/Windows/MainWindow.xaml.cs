﻿using GraphApp.Core;
using GraphApp.Core.GraphApplications;
using GraphApp.Core.GraphRepresentation;
using GraphApp.Core.GraphRepresentation.Berg;
using GraphApp.Core.GraphRepresentation.Description;
using GraphApp.Core.Handlers;
using GraphApp.UI.Controls;
using Microsoft.Win32;
using QuickGraph;
using QuickGraph.Graphviz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitializeGraph();
            DisplayInformation();
            InitializeOperations();
        }

        private void InitializeGraph()
        {
            GraphInformation.Graph = new BergDirected();
        }

        public void InitializeOperations()
        {
            var selectedIndex = cbOperations.SelectedIndex;
            cbOperations.Items.Clear();
            var generalOps = EnumHelper.GetAttributeDescriptions<GraphInformation.GeneralOperations>();
            foreach (var label in generalOps)
                cbOperations.Items.Add(label);
            if (GraphInformation.Graph is BergUndirected)
            {
                var undirOps = EnumHelper.GetAttributeDescriptions<GraphInformation.UndirectedOperations>();
                foreach (var label in undirOps)
                    cbOperations.Items.Add(label);
            }
            cbOperations.SelectedIndex = 
                selectedIndex == -1 || selectedIndex > cbOperations.Items.Count ? 0 : selectedIndex;
        }

        private void btnImportGraph_Click(object sender, RoutedEventArgs e)
        {
            var fileName = FileHandler.GetGraphFile();
            if (String.IsNullOrEmpty(fileName))
                return;
            var inputTypeWindow = new InputTypeWindow(fileName);
            inputTypeWindow.Show(grdExtra, grdMain);
        }

        public void DisplayInformation()
        {
            var graph = GraphInformation.Graph;
            runEdgesCount.Text = graph.EdgesCount.ToString();
            runEdgesPower.Text = graph.GraphPower().ToString();
            runVertexCount.Text = graph.VertexCount().ToString();
            var diameter = graph.Diameter();
            var radius = graph.Radius();
            runDiameter.Text = diameter == Double.MaxValue ? "∞" : graph.Diameter().ToString();
            runRadius.Text = radius == Double.MaxValue ? "∞" : graph.Radius().ToString();

            if (graph is BergDirected)
            {
                lblConnectivityComponent.Visibility =
                    lblCyclomaticNumber.Visibility =
                    lblIsContainCycle.Visibility =
                    lblIsTree.Visibility = Visibility.Collapsed;
                runIsDirected.Text = "Да";
            }
            else if (graph is BergUndirected undirGraph)
            {
                lblConnectivityComponent.Visibility =
                    lblCyclomaticNumber.Visibility =
                    lblIsContainCycle.Visibility =
                    lblIsTree.Visibility = Visibility.Visible;
                runIsDirected.Text = "Нет";
                runConnectivityComponent.Text = undirGraph.ConnectivityComponents().ToString();
                runCyclomaticNumber.Text = undirGraph.CyclomaticNumber().ToString();
                runIsContainCycle.Text = undirGraph.IsContainCycle() ? "Да" : "Нет";
                runIsTree.Text = undirGraph.IsTree() ? "Да" : "Нет";
            }
            GraphInformation.Graph.Visualize();
            UpdateImage();
            InitializeOperations();
        }

        private void UpdateImage()
        {
            imgGraph.Source = null;
            imgGraph.UpdateLayout();
            var img = new BitmapImage();
            img.BeginInit();
            img.UriSource = new Uri(GraphInformation.Graph.Visualize());
            img.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.EndInit();
            img.Freeze();
            imgGraph.Dispatcher.BeginInvoke(new Action(() => imgGraph.Source = img));
        }

        private void cbOperations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            grdOperations.Children.Clear();
            switch (cbOperations.SelectedIndex)
            {
                case (int)GraphInformation.GeneralOperations.AddVertex:
                case (int)GraphInformation.GeneralOperations.FindAdjVertices:
                case (int)GraphInformation.GeneralOperations.RemoveVertex:
                case (int)GraphInformation.GeneralOperations.VertexList:
                case (int)GraphInformation.GeneralOperations.GraphType:
                case (int)GraphInformation.UndirectedOperations.VertexListByDFS:
                case (int)GraphInformation.UndirectedOperations.PrintCycle:
                case (int)GraphInformation.UndirectedOperations.IsPlanar:
                case (int)GraphInformation.UndirectedOperations.AbsoluteCenter:
                    var vertexOpsWindow = new VertexOpsWindow(cbOperations.SelectedIndex);
                    vertexOpsWindow.Show(grdOperations);
                    break;
                case (int)GraphInformation.GeneralOperations.AddEdge:
                case (int)GraphInformation.GeneralOperations.FindEdge:
                case (int)GraphInformation.GeneralOperations.RemoveEdge:
                    var edgeOpsWindow = new EdgeOpsWindow((GraphInformation.GeneralOperations)cbOperations.SelectedIndex);
                    edgeOpsWindow.Show(grdOperations);
                    break;
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

    }
}
